/**
 * http://usejsdoc.org/
 */
var logger = require('log4js').getLogger();

/**
 * 
 * 
 */
var WorkerState = Object.freeze({
	"UNKNOWN":0, 
	"DISCONNECTED":1, 
	"WAITING":2, 
	"STARTED":3, 
	"RUNNING":4, 
	"FINISHED":5}); // maybe FINISHED is unusefull

/**
 * 
 * 
 */
var WorkerStateName = Object.freeze([
	"UNKNOWN", 
	"DISCONNECTED", 
	"WAITING", 
	"STARTED", 
	"RUNNING", 
	"FINISHED"] // maybe FINISHED is unusefull
);


/**
 * 
 * 
 */
class Worker {
	/**
	 * 
	 */
	constructor(conn) {
		this._connection = conn
		this._state = WorkerState["UNKNOWN"]
		this._client = this._connection.clientProxy;
		this._delay = 0
	}

	/**
	 * 
	 */
	id() {
		return this._connection.id
	}
	
	/**
	 * 
	 */
	address() {
		return this._connection.socket.address
	}

	/**
	 * 
	 */
	delay() {
		return this._delay
	}

	/**
	 * 
	 */
	wait() {
		if (!this.disconnected()) {
			this._state = WorkerState["WAITING"]
		}
	}

	/**
	 * 
	 */
	disconnect() {
		this._state = WorkerState["DISCONNECTED"]
	}

	/**
	 * 
	 */
	finish() {  // maybe FINISHED is unusefull
		if (!this.disconnected()) {
			this._state = WorkerState["FINISHED"]
		}
	}

	/**
	 * 
	 */
	start() {
		this._state = WorkerState["STARTED"]
	}

	/**
	 * 
	 */
	waiting() {
		return (this._state == WorkerState["WAITING"])
	}

	/**
	 * 
	 */
	disconnected() {
		return (this._state == WorkerState["DISCONNECTED"])
	}

	/**
	 * 
	 */
	started() {
		return (this._state == WorkerState["STARTED"])
	}

	/**
	 * 
	 */
	running() {
		return (this._state == WorkerState["RUNNING"])
	}

	/**
	 * 
	 */
	toString() {
		return "Worker[" + this.id() + ", " + WorkerStateName[this._state] + "]"
	}
	
	/**
	 * 
	 */
	info() {
		return this.toString()
	}

	/**
	 * 
	 */
	exec(task) {
		this._state = WorkerState["RUNNING"]
		if (logger.level == 'DEBUG') {
			logger.debug("Worker, exec, this: " + this)
		}
		var p = task.exec(this)
		if (logger.level == 'DEBUG') {
			logger.debug("Worker, exec, this: " + this + ", p: " + JSON.stringify(p))
		}
		return p
	}
	
	/**
	 * 
	 */
	setdelay(d) {
		this._delay = d
	}
}

/**
 * exports
 * 
 */
module.exports = {
		'WorkerState':WorkerState,
		'WorkerStateName':WorkerStateName,
		'Worker':Worker
}


