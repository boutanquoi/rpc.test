/**
 * http://usejsdoc.org/
 */

const EventEmitter = require('events');

var logger = require('log4js').getLogger();

const millisec = require("./function").millisec

/**
 * 
 * 
 */
var TaskState = Object.freeze({
	"UNKNOWN":0, 
	"STARTED":1, 
	"OK":2, 
	"ERROR":3});

/**
 * 
 */
var TaskStateName = Object.freeze([
	"UNKNOWN", 
	"STARTED", 
	"OK", 
	"ERROR"]
);

/**
 * 
 * 
 */
class Task {
	/**
	 * 
	 */
	constructor(f) {
		this._id = ("Task#" + Math.random()).replace("0.","")
		this.callback = f
		this._state = TaskState["UNKNOWN"]
		this.created_on = millisec()
		this.started_on = undefined
		this.finished_on = undefined
	}

	/**
	 * 
	 */
	id() {
		return this._id
	}

	/**
	 * 
	 */
	started() {
		return (this._state == TaskState["STARTED"])
	}

	/**
	 * 
	 */
	isOk() {
		return (this._state == TaskState["OK"])
	}

	/**
	 * 
	 */
	isError() {
		return (this._state == TaskState["ERROR"])
	}

	/**
	 * 
	 */
	isSameWorker(w) {
		return (this.worker != undefined &&  w.id() == this.worker.id())
	}

	/**
	 * 
	 */
	exec(w) {
		this.started_on = millisec()
		this._state = TaskState["STARTED"]
		this.worker = w
		return this.callback(this.worker._client)
	}

	/**
	 * 
	 */
	ok(w) {
		this.finished_on = millisec()
		this._state = TaskState["OK"]
		this.okWorker = w.id()
	}

	/**
	 * 
	 */
	error() {
		this.finished_on = millisec()
		this._state = TaskState["ERROR"]
		if (this.worker != undefined) {
			this.errorWorker = this.worker.id()
		}
	}

	/**
	 * 
	 */
	toString() {
		var jid = undefined
		if (this.job) {
			jid = this.job.id()
		}

		return "Task[id: " + this.id() + ", jid: " + jid + ", state: " + TaskStateName[this._state] + ", worker: " + this.worker + "]"
	}

	/**
	 * 
	 */
	info() {
		var worker = undefined
		var delay_scheduled = undefined
		var delay_running = undefined

		if (this.started()) {
			worker = this.worker
			delay_scheduled = this.started_on - this.created_on
		} else if (this.isOk()) {
			worker = this.okWorker
			delay_scheduled = this.started_on - this.created_on
			delay_running = this.finished_on - this.started_on
		} else if (this.isError()) {
			worker = this.errorWorker
			delay_scheduled = this.started_on - this.created_on
			delay_running = this.finished_on - this.started_on
		}
		
		var jid = undefined
		if (this.job) {
			jid = this.job.id()
		}

		return "Task[id: " + this.id() + ", jid: " + jid + ", state: " + TaskStateName[this._state] + ", worker: " + worker + ", delay_scheduled: " + delay_scheduled + ", delay_running: " + delay_running + "]"
	}

}

module.exports = Task;
