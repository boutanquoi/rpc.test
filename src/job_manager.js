/**
 * http://usejsdoc.org/
 */
const ResourceManager = require('../src/task_manager.js')

var logger = require('log4js').getLogger();

/**
 * 
 * 
 */
class JobManager {
	/**
	 * 
	 */
	constructor(tm) {
		this.taskmanager = tm
		this.jobs = {}
	}
	
	/**
	 * 
	 */
	getTaskManager() {
		return this.taskmanager
	}
	
	/**
	 * 
	 */
	getJobs() {
		return this.jobs
	}
	
	/**
	 * 
	 */
	newJob(jobclass, request) {
		var job = new jobclass(this.taskmanager, request)
		this.jobs[job.id()] = job
		logger.info("JobManager, job: " + job)
		return job
	}
	
	/**
	 * 
	 */
	run(job) {
		job.run()
	}
	
	/**
	 * 
	 */
	toString() {
		return "JobManager[taskmanager: " + this.taskmanager + "]";
	}
}

module.exports = JobManager;
