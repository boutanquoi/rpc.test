/**
 * http://usejsdoc.org/
 */
var logger = require('log4js').getLogger();

/**
 * Web socket RPC
 * 
 */
class WSRPC {
	/**
	 * 
	 */
	constructor() {
		logger.info("")
	}
	
	/**
	 * 
	 */
	init(host, port) {
		this._host = host
		this._port = port
		
		logger.info("host: " + this._host)
		logger.info("port: " + this._port)
		
		var WebSocketServer = require('rpc-websockets').Server
		this.server = new WebSocketServer({
		  port: this._port,
		  host: this._host
		})
		
		this.server.register('sum', function(params) {
		  return params[0] + params[1]
		})
	}
	
	/**
	 * 
	 */
	host() {
		return this._host
	}
	
	/**
	 * 
	 */
	port() {
		return this._port
	}
	
	/**
	 * 
	 */
	toString(){
    return 'WSRPC(' + this.host() + ":" + this.port() + ")";
  }

}

module.exports = new WSRPC();
