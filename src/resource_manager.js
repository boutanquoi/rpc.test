/**
 * http://usejsdoc.org/
 */
const EventEmitter = require('events');
const Worker = require("./worker").Worker

var logger = require('log4js').getLogger();

const millisec = require("./function").millisec

/**
 * 
 * 
 */
class ResourceManager extends EventEmitter {
	/**
	 * 
	 */
	constructor() {
		super()
		this.id = ("ResourceManager#" + Math.random()).replace("0.","")
		this.workers = {}
		this.available = []
		this.taskmanager = null
		this.init_events()
	}

	/**
	 * 
	 */
	getWorkers() {
		return this.workers
	}

	/**
	 * 
	 */
	getAvailable() {
		return this.available
	}

	/**
	 * 
	 */
	subscribe(rm) {
		this.taskmanager = rm
	}

	/**
	 * 
	 */
	newWorker(conn) {
		var worker = new Worker(conn)
		worker.setdelay(0) // make it available as soon as it finish
		worker.wait()
		this.workers[worker.id()] = worker
		this.available.push(worker)
		logger.info("ResourceManager, newWorker, worker: " + worker + ", this: " + this)
		if (this.taskmanager) {
			this.taskmanager.emit('newWorker', worker)
		}
	}

	/**
	 * 
	 */
	delWorker(conn) {
		var worker = this.workers[conn.id]
		worker.disconnect()
		delete this.workers[worker.id()];
		var index = this.available.findIndex(function(x) {return x.id() == worker.id()})
		if (index > -1) {
			this.available.splice(index, 1);
		}
		logger.info("ResourceManager, delWorker, worker: " + worker + ", this: " + this)
		if (this.taskmanager) {
			this.taskmanager.emit('delWorker', worker)
		}
	}

	/**
	 * 
	 */
	init_events() {
		this.on('newTask', this.onNewTask)
		this.on('releaseWorker', this.onReleaseWorker)
	}

	/**
	 * 
	 */
	onNewTask(tm, task) {
		if (logger.level == 'DEBUG') {
			logger.debug("ResourceManager, onNewTask, this: " + this)
		}
		let _this = this

		var p = new Promise(
				function(resolve, reject) {
					var worker = _this.get_worker()
					if (worker) {
						resolve({'worker':worker, 'task':task})
					} else {
						reject({'error':'noworker', 'task':task})
					}
				})
		.then(
				function({'worker':worker, 'task':task}) {
					return worker.exec(task)
				})
				.then(
						function (result) {
							
							var worker = task.worker
							setTimeout(function() {
								_this.emit('releaseWorker', worker)
							}, worker.delay())
							
							tm.emit('taskSuccess', task)
							result.call_time = task.started_on
							return result
						})
//						.catch(
//								function(error) {
//									logger.error("ResourceManager, onNewTask, error: " + JSON.stringify(error))
//									tm.emit('taskError',task)
//									return error
//								})

		return p
	}

	/**
	 * 
	 */
	onReleaseWorker(worker) {
		if (!worker.disconnected()) {
			this.available.push(worker)
			worker.wait()
		}
		if (logger.level == 'DEBUG') {
			logger.debug("ResourceManager, onReleaseWorker, this: " + this + ", worker: " + worker)
		}
	}
	
	/**
	 * 
	 */
	get_worker() {
		return do_get(this)
	}

	/**
	 * 
	 */
	toString() {
		return this.constructor.name + "[id: " + this.id + ", available: " + this.available.length + ", workers: " + Object.keys(this.workers).length  + "]"
	}
	
}

/**
 * 
 */
function do_get(rm) {
	var found = 0

	require('deasync').loopWhile(function(){
		var n_available = rm.available.length
		// look for a worker
		while (found == 0 && n_available > 0) {
			var w = rm.available.shift();
			if (w.waiting()) {
				found = w
			} else if (!w.disconnected()) {
				rm.available.push(w)
			} else {
			}
			n_available--
		}

		return !found;
	})

	return found
}

module.exports = ResourceManager;

