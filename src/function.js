
/**
 * 
 * @returns
 */
function millisec() {
	var t = process.hrtime()
	return t[0] * 1000. + t[1] / 1000. / 1000.
}


/**
 * 
 * @param length
 * @returns
 */
function randomStr(length) {
	if (length === void 0) { length = 10; }
	var rs, i, nextIndex, l, chars = [
		'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
		'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
		'1', '2', '3', '4', '5', '6', '7', '8', '9', '0'];
	rs = '';
	for (i = 0; i < length; i++) {
		nextIndex = Math.floor(Math.random() * chars.length);
		rs += chars[nextIndex];
	}
	return rs;
};


module.exports = {
		'millisec':millisec,
		'randomStr':randomStr
};
