var express = require('express');
var path = require('path');
var logger = require('morgan');
var bodyParser = require('body-parser');

const index = require('../routes/index');
const jobs = require('../routes/jobs');
const tasks = require('../routes/tasks');
const workers = require('../routes/workers');

var app = express();

// view engine setup
app.set('views', path.join('views'));
app.set('view engine', 'jade');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// static route
app.use(express.static(path.resolve('public/')));
app.use('/test', express.static(path.resolve('test/')));
app.use('/eureca.js', express.static(path.resolve('lib/eureca.io/lib/eureca.js')));

// dynamic route
app.use('/jobs', jobs);
app.use('/tasks', tasks);
app.use('/workers', workers);
app.use('/', index);

// comment following because of eureca.io that generate /eureca.js
//
// catch 404 and forward to error handler
// app.use(function(req, res, next) {
// var err = new Error('Not Found');
// err.status = 404;
// next(err);
// });
//

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
