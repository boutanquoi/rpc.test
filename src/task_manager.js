/**
 * http://usejsdoc.org/
 */
const EventEmitter = require('events');
const Task = require("./task")

var logger = require('log4js').getLogger();

const millisec = require("./function").millisec

/**
 * 
 * 
 */
class TaskManager extends EventEmitter {
	/**
	 * 
	 */
	constructor(rm) {
		super()
		this.id = ("TaskManager#" + Math.random()).replace("0.","")
		this.resourcemanager = rm
		this.init_events()
		this.submitted = {}
		this.completed = []
		this.in_error = []
	}

	/**
	 * 
	 */
	getResourceManager() {
		return this.resourcemanager
	}

	/**
	 * 
	 */
	getSubmittedTask() {
		return this.submitted
	}

	/**
	 * 
	 */
	getCompletedTask() {
		return this.completed
	}

	/**
	 * 
	 */
	getInErrorTask() {
		return this.in_error
	}

	/**
	 * 
	 */
	exec(f, job) {
		if (logger.level == 'DEBUG') {
			logger.debug("TaskManager, exec, this: " + this)
		}
		var task = new Task(f)
		task.job = job
		this.submitted[task.id()] = task
		var p = this.resourcemanager.onNewTask(this, task)
		if (logger.level == 'DEBUG') {
			logger.debug("TaskManager, exec, this: " + this + ", p: " + (typeof p))
		}
		return p
	}

	/**
	 * 
	 */
	init_events() {
		this.on('taskError', this.onTaskError)
		this.on('taskSuccess', this.onTaskSuccess)
		this.on('newWorker', this.onNewWoker)
		this.on('delWorker', this.onDelWoker)
	}

	/**
	 * 
	 */
	onTaskSuccess(task) {
		if (logger.level == 'DEBUG') {
			logger.debug("TaskManager, onTaskSuccess, task: " + task)
		}
		var worker = task.worker
		task.worker = undefined
		task.ok(worker)
		delete this.submitted[task.id()];
		this.completed.push(task)
//		if (worker) {
//			this.resourcemanager.emit('releaseWorker', worker)
//		}
	}

	/**
	 * 
	 */
	onTaskError(task) {
		logger.error("TaskManager, onTaskError, task: " + task)
		var worker = task.worker
		task.worker = undefined
		task.error(worker)
		delete this.submitted[task.id()];
		this.in_error.push(task)
		if (worker) {
			this.resourcemanager.emit('releaseWorker', worker)
		}
	}

	/**
	 * 
	 */
	onNewWoker(worker) {
	}

	/**
	 * 
	 */
	onDelWoker(worker) {
		logger.error("TaskManager, onDelWoker, worker: " + worker)
		for ( var key in this.submitted) {
			var task = this.submitted[key]
			if (task.isSameWorker(worker)) {
				this.emit('taskError', task)
			}
		}
	}

	/**
	 * 
	 */
	toString() {
		return "TaskManager[id: " + this.id  + "]"
	}
}

module.exports = TaskManager;

