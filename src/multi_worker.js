/**
 * http://usejsdoc.org/
 */
var logger = require('log4js').getLogger();
const WorkerJS = require("./worker")
const Worker = WorkerJS.Worker
const WorkerState = WorkerJS.WorkerState
const WorkerStateName = WorkerJS.WorkerStateName
const randomStr = require("./function").randomStr


/**
 * 
 * 
 */
class MultiWorker extends Worker {
	/**
	 * 
	 */
	constructor(conn) {
		super(conn)
		this._address = conn.socket.address
		this.workers = {}
		this.available = []
		this._id = randomStr(16)
	}

	/**
	 * 
	 */
	id() {
		return this._id
	}
	
	/**
	 * 
	 */
	address() {
		return this._address
	}
	
	/**
	 * 
	 */
	add(conn) {
		if (conn.socket.address.ip == this.address().ip) {
			var worker = new Worker(conn)
			worker.setdelay(0) // make it available as soon as it finish
			worker.wait()
			this.workers[worker.id()] = worker
			this.available.push(worker)
			logger.info("MultiWorker, add, worker: " + worker + ", this: " + this)
		}
	}
	
	/**
	 * 
	 */
	del(conn) {
		var worker = this.workers[conn.id]
		worker.disconnect()
		delete this.workers[worker.id()];
		var index = this.available.findIndex(function(x) {return x.id() == worker.id()})
		if (index > -1) {
			this.available.splice(index, 1);
		}
		logger.info("MultiWorker, del, worker: " + worker + ", this: " + this)
	}
	
	/**
	 * 
	 */
	release(worker) {
		if (!worker.disconnected()) {
//			this.available.push(worker)
			worker.wait()
		}
		if (logger.level == 'DEBUG') {
			logger.debug("MultiWorker, release, this: " + this + ", worker: " + worker)
		}
	}

	/**
	 * 
	 */
	wait() {
	}

	/**
	 * 
	 */
	finish() {
	}

	/**
	 * 
	 */
	start() {
	}

	/**
	 * 
	 */
	empty() {
		return this.size <= 0
	}
	
	/**
	 * 
	 */
	waiting() {
		var rst = true
		
		Object.values(this.workers).forEach(function(x) {
			rst = rst && !(x.started() || x.running())
		})
		
		return (rst && Object.keys(this.workers).length > 0)
	}

	/**
	 * 
	 */
	disconnected() {
		var rst = true
		Object.keys(this.workers).forEach(function(x) {
			rst = rst && x.disconnected()
		})
		return rst
	}

	/**
	 * 
	 */
	started() {
		var rst = false
		Object.keys(this.workers).forEach(function(x) {
			rst = rst || x.started()
		})
		return rst
	}

	/**
	 * 
	 */
	running() {
		var rst = false
		Object.keys(this.workers).forEach(function(x) {
			rst = rst || x.running()
		})
		return rst
	}

	/**
	 * 
	 */
	toString() {
		return "MultiWorker[" + this.id() + "]"
	}
	
	/**
	 * 
	 */
	info() {
		var rst = "MultiWorker[" + this.id() + ", available: " + this.available.length + ", workers: " + Object.keys(this.workers).length + ", workers:\n"
		
		for (var key in this.workers) {
			rst += this.workers[key].info()
			rst += "\n"
		}
		
		rst += "]"
		
		return rst
	}

	/**
	 * 
	 */
	exec(task) {
		if (logger.level == 'DEBUG') {
			logger.debug("Worker, exec, this: " + this)
		}
		var worker = Object.values(this.workers).find(function(x) {
		  return x.waiting();
		});
		var p = task.exec(worker)
		if (logger.level == 'DEBUG') {
			logger.debug("Worker, exec, this: " + this + ", p: " + JSON.stringify(p))
		}
		return p
	}
	
	/**
	 * 
	 */
	setdelay(d) {
		this._delay = d
	}
}

module.exports = MultiWorker;


