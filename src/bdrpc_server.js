/**
 * http://usejsdoc.org/
 */
var logger = require('log4js').getLogger();

const Eureca = require("../lib/eureca.io")

/**
 * Bidirectional RPC
 * 
 */
class BDRPC {

	/**
	 * 
	 */
	constructor(_express, rm) {
		logger.info("")
		this.express = _express
		logger.info("BDRPC, express: " + this.express)
		this.ressourceManager = rm
		logger.info("BDRPC, taskManager: " + this.ressourceManager)
		logger.info("BDRPC, Eureca: " + Eureca)

		this.server = new Eureca.Server({allow:['sub','minibatch'], debuglevel: 4});
		this.server.attach(this.express)

		this.server.onConnect(this.onConnect());
		this.server.onDisconnect(this.onDisconnect());
		this.server.onError(this.onError());
		this.server.onMessage(this.onMessage());

		this.export_method()
	}

	/**
	 * 
	 */
	express() {
		return this.express
	}

	/**
	 * 
	 */
	export_method() {
		this.server.exports.hello_from = function (client) {
			logger.info('hello_from, client: ' + client);
		}
		this.server.exports.hello = function () {
			logger.info('hello, client');
		}
	}

	/**
	 * 
	 */
	toString(){
		return 'BDRPC(' + JSON.stringify(this.express.address()) + ")";
	}

	/**
	 * 
	 */
	onConnect() {
		return function(conn) {
			logger.info("BDRPC, onConnect, conn: " + conn.id + ", conn.socket: " + JSON.stringify(conn.socket) + ", conn.socket.address: " + JSON.stringify(conn.socket.address))
			if (logger.level == 'DEBUG') {
				logger.debug("BDRPC, onConnect, conn: " + conn.id + ", this: " + this)
			}
			this.ressourceManager.newWorker(conn)
		}.bind(this)
	}

	/**
	 * 
	 */
	onDisconnect(conn) {
		return function(conn) {
			if (logger.level == 'DEBUG') {
				logger.debug("BDRPC, onDisconnect, conn: " + conn.id + ", this: " + this)
			}
			this.ressourceManager.delWorker(conn)
		}.bind(this)
	}

	/**
	 * 
	 */
	onError(conn) {
		return function(error, conn) {
			logger.error("BDRPC, onError, conn: " + conn.id + ", this: " + this + ", ERROR: " + error)
		}.bind(this)
	}

	/**
	 * 
	 */
	onMessage(conn) {
		return function(msg, conn) {
			if (logger.level == 'DEBUG') {
				logger.debug("BDRPC, onMessage, conn: " + conn.id + ", this: " + this + ", MSG: " + msg)
			}
		}.bind(this)
	}
}

module.exports = BDRPC;
