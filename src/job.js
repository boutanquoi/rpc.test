/**
 * http://usejsdoc.org/
 */

var logger = require('log4js').getLogger();

/**
 * 
 * 
 */
class Job {
	/**
	 * 
	 */
	constructor(tm, req) {
		this.taskmanager = tm
		this._id = (this.constructor.name + "#" + Math.random()).replace("0.","")
		this._request= req
	}
	
	/**
	 * 
	 */
	id() {
		return this._id
	}
	
	/**
	 * 
	 */
	request() {
		return this._request
	}
	
	/**
	 * 
	 */
	toString() {
		return "" + this.constructor.name + "[" + this._id + "]"
	}

}

module.exports = Job;
