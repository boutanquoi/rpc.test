/**
 * http://usejsdoc.org/
 */
var logger = require('log4js').getLogger();

const MultiWorker = require("./multi_worker")
const ResourceManager = require("./resource_manager")

const millisec = require("./function").millisec

/**
 * 
 * 
 */
class ByHostResourceManager extends ResourceManager {
	/**
	 * 
	 */
	constructor() {
		super()
		this.id = ("ByHostResourceManager#" + Math.random()).replace("0.","")
	}

	/**
	 * 
	 */
	newWorker(conn) {
		var new_one = false
		var multi_worker = this.workers[conn.socket.address.ip]
		if (typeof multi_worker === "undefined") {
			multi_worker = new MultiWorker(conn)
			multi_worker.setdelay(0) // make it available as soon as it finish
			multi_worker.wait()
			this.workers[conn.socket.address.ip] = multi_worker
			this.available.push(multi_worker)
			new_one = true
		}
		multi_worker.add(conn)
		
		if (new_one) {
			if (this.taskmanager) {
				this.taskmanager.emit('newWorker', multi_worker)
			}
		}
		
		logger.info("ResourceManager, newWorker, worker: " + multi_worker + ", this: " + this)
	}

	/**
	 * 
	 */
	delWorker(conn) {
		var multi_worker = this.workers[conn.socket.address.ip]
		if (typeof multi_worker !== "undefined") {
			
			var worker = multi_worker.del(conn)
			if (multi_worker.empty()) {
				delete this.workers[conn.socket.address.ip];
				var index = this.available.findIndex(function(x) {return x.id() == multi_worker.id()})
				if (index > -1) {
					this.available.splice(index, 1);
				}
				logger.info("ResourceManager, delWorker, worker: " + worker + ", this: " + this)
				if (this.taskmanager) {
					this.taskmanager.emit('delWorker', worker)
				}
			}
			
		}
	}

	/**
	 * 
	 */
	onReleaseWorker(worker) {
		if (!worker.disconnected()) {
			
			var multi_worker = this.workers[worker.address().ip]
			
			if (typeof multi_worker === "undefined") {
				logger.debug("ResourceManager, onReleaseWorker, this: " + this + ", rebuild multi-worker for: " + worker)
				multi_worker = new MultiWorker(worker.conn)
				multi_worker.setdelay(0) // make it available as soon as it finish
				multi_worker.wait()
				this.workers[worker.address().ip] = multi_worker
				this.available.push(multi_worker)
			}
			this.available.push(multi_worker)
			multi_worker.release(worker)
		}
		if (logger.level == 'DEBUG') {
			logger.debug("ResourceManager, onReleaseWorker, this: " + this + ", worker: " + worker)
		}
	}
	
	/**
	 * 
	 */
	get_worker() {
		return do_get(this)
	}
}

/**
 * 
 */
function do_get(rm) {
	var found = 0

	require('deasync').loopWhile(function(){
		var n_available = rm.available.length
		// look for a worker
		while (found == 0 && n_available > 0) {
			var w = rm.available.shift();
			if (w.waiting()) {
				found = w
			} else if (!w.disconnected()) {
				rm.available.push(w)
			} else {
			}
			n_available--
		}

		return !found;
	})

	return found
}

module.exports = ByHostResourceManager;

