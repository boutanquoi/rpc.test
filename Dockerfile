#
# Ubuntu Dockerfile
#
# https://github.com/dockerfile/ubuntu
#

# Pull base image.
FROM ubuntu:16.04

# Install.
RUN \
  sed -i 's/# \(.*multiverse$\)/\1/g' /etc/apt/sources.list && \
  apt-get update && \
  apt-get -y upgrade && \
  apt-get install -y build-essential && \
  apt-get install -y software-properties-common && \
  apt-get install -y byobu curl git htop man unzip vim wget net-tools && \
  rm -rf /var/lib/apt/lists/*
  
RUN apt-get update && apt-get install -y openssh-server openssh-client

# For info
RUN mkdir /etc/BUILDS

RUN \
    cd /etc/BUILDS && \
    curl -sL https://deb.nodesource.com/setup_8.x | bash - && \
    apt-get install -y nodejs

# Add files.
ADD docker/root/.bashrc /root/.bashrc
ADD docker/root/.gitconfig /root/.gitconfig
ADD docker/root/.scripts /root/.scripts
ADD docker/root/start.sh /root/start.sh

RUN mkdir /root/.ssh

RUN echo "root:docker" | chpasswd && \

    ssh-keygen -t rsa -N "docker" -f "/root/.ssh/id_dsa" && \
    
    echo "Host *" > /root/.ssh/config && \
    echo "\tIdentityFile /root/.ssh/id_rsa" >> /root/.ssh/config && \
    
    chmod -R 700 /root/.ssh

# Set environment variables.
ENV HOME /root

RUN mkdir /root/rpc.test
    
COPY bin  /root/rpc.test/bin
COPY lib  /root/rpc.test/lib
COPY misc  /root/rpc.test/misc
COPY node_modules  /root/rpc.test/node_modules
COPY public  /root/rpc.test/public
COPY routes  /root/rpc.test/routes
COPY src  /root/rpc.test/src
COPY test  /root/rpc.test/test
COPY views  /root/rpc.test/views


# Define working directory.
WORKDIR /root

EXPOSE 443

# Define default command.
CMD /root/start.sh
