/**
 * http://usejsdoc.org/
 */


/**
 * 
 * @param delay
 * @returns
 */
function sleep(delay) {
	var start = new Date().getTime();
	while (new Date().getTime() < start + delay) {
	}
}


/**
 * 
 */
onmessage = function(m) {
	client.onMessage(m)
}

/**
 * 
 */
onerror = function(m) {
	client.onError(m)
}

/**
 * 
 * 
 */
class Client {
	/**
	 * 
	 */
	constructor(ww) {
		this.webworker = ww
		if (this.webworker) {
			this.webworker.client = this
		}

		this.client = new Eureca.Client({
			uri : 'https://localhost:3000/',
			prefix : 'eureca.io',
			retry : 0
		});
		console.log('BUILD, this.client: ' + this.client + ", webworker: " + this.webworker);
	}

	/**
	 * 
	 */
	ready() {
		this.api()
		console.log('READY');
		this.client.ready(function(proxy) {
		});
	}

	/**
	 * 
	 */
	flatten(array) {
		let _this = this
		return array.reduce(function(acc, x) {
			if (Array.isArray(x)) {
				return acc + _this.flatten(x)
			} else {
				return acc + x
			}
		}, 0.)
	}
	
	/**
	 * 
	 */
	api() {
		let _this = this
		this.client.exports = {
				sub : function(a, b, r) {
					var delay = 100
					if (r) {
						delay = r
					}
					console.log("SUB, HELLO, date:" + new Date().getTime() + ", delay: " + delay);
					var t0 = new Date().getTime()
					sleep(delay)
					var t1 = new Date().getTime()
					console.log("SUB, BYE, date:" + new Date().getTime());
					return {'result':(a - b), 'delay':delay, 'real':(t1-t0)};
				},
				minibatch : function(mb) {
					var t0 = new Date().getTime()
					
					try {
						console.log("MINIBATCH, HELLO, date:" + new Date().getTime() + ", mb: " + mb.length);
						console.log("MINIBATCH, HELLO, date:" + new Date().getTime() + ", mb: " + mb);
						
						var y = _this.flatten(mb);
//						console.log("MINIBATCH, HELLO, date:" + new Date().getTime() + ", merged: " + merged.length);
//						var y = merged.reduce(function(acc, x) {return acc + x}, 0)
						console.log("MINIBATCH, HELLO, date:" + new Date().getTime() + ", y: " + y);
						var t1 = new Date().getTime()
						return {'result':y, 'delay':(t1-t0)};
					} catch (error) {
						var t1 = new Date().getTime()
						console.log(error)
						return {'error':error, 'delay':(t1-t0)}
					}
				}
		}
	}

	/**
	 * 
	 */
	onMessage(message) {
		console.log('Client, onMessage, this: ' + this + ", message: " + message.data);
	}

	/**
	 * 
	 */
	onError(message) {
		console.log('Client, onError, this: ' + this + ", message: " + message);
	}

	/**
	 * 
	 */
	unload() {
		console.log('Unload');
	}

	/**
	 * 
	 */
	toString() {
		if (this.client) {
			return "Client[serverProxy: " + this.client.serverProxy  + ", ready: " + this.client.isReady() + "]"
		} else {
			return "Client[]"
		}
	}
}

console.log("index, this: " + this.constructor.name)
this.window=undefined
this.importScripts('/eureca.js');
var client = new Client(this)
console.log("index, client:  " + client)
client.ready()


