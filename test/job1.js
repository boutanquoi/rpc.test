/**
 * http://usejsdoc.org/
 */
const Async = require('async-kit')
const Job = require("../src/job.js")


var logger = require('log4js').getLogger();

function millisec() {
	var t = process.hrtime()
	return t[0] * 1000. + t[1] / 1000. / 1000.
}

/**
 * 
 * 
 */
class Job1 extends Job {
	/**
	 * 
	 */
	constructor(tm, req) {
		super(tm, req)
		logger.info("Job1, this: " + this + ", req.query: " + JSON.stringify(req.query))
	}

	/**
	 * 
	 */
	sub(i, delay) {
		return function(client) {
			return client.sub(10,i,delay * Math.random())
		}
	}

	/**
	 * 
	 */
	async run() {
		let _this = this
		
		var i = 0;
		var N = 10
		var delay = 1000
		
		if (this.request().query.N) {
			N = this.request().query.N
		}
		if (this.request().query.delay) {
			delay = this.request().query.delay
		}
		
		logger.info("Job1, run, this: " + this + ", N: " + N + ", mean delay: " + delay)
		
		var total_time = 0

		Async.while(
				function(error, results, callback) {
					i++
					callback(undefined, i <= N)
				} )
			.do(
					{
						'call':function(callback) {
							var t0 = millisec()
							_this.taskmanager.exec(_this.sub(i, delay), _this).then(
									function(rst) {
										var t1 = millisec()
										logger.info("Job, run, this: " + _this + ", rst: " + JSON.stringify(rst) + ", duration: " + (t1 - t0) + ", stack latency: " + (t1 - t0 - rst.real) + ", call latency: " + (t1 - rst.call_time - rst.real))
										total_time += rst.real
										callback(undefined, i)
									}).catch(
											function(error) {
												logger.error("Job, run, this: " + _this + ", error: " + JSON.stringify(error))
												callback(undefined, i)
											}
									)
						}
					})
					.exec( function(error, results) {
						logger.info("Job1, run, this: " + _this + ", N: " + N + ", total time: " + total_time)
					});
	}
}

module.exports = Job1;
