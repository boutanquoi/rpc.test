/**
 * http://usejsdoc.org/
 */
const Async = require('async-kit')
const Job = require("../src/job.js")
const DataSource = require('./datasource');

var logger = require('log4js').getLogger();

function millisec() {
	var t = process.hrtime()
	return t[0] * 1000. + t[1] / 1000. / 1000.
}

/**
 * 
 * 
 */
class JobDL extends Job {
	/**
	 * 
	 */
	constructor(tm, req) {
		super(tm, req)
		logger.info("JobDL, this: " + this + ", req.query: " + JSON.stringify(req.query))
		this.dataSource = new DataSource( 'example.db' );
	}


	/**
	 * 
	 */
	minibatch(desc) {
		return function(client) {
			return client.minibatch(desc)
		}
	}

	/**
	 * 
	 */
	async run() {
		let _this = this
		
		await this.dataSource.init();
		
		var i = 0;
		var N = 10
		var delay = 1000
		
		if (this.request().query.N) {
			N = this.request().query.N
		}
		if (this.request().query.delay) {
			delay = this.request().query.delay
		}
		
		logger.info("JobDL, run, this: " + this + ", N: " + N + ", mean delay: " + delay)
		
		var total_time = 0

		Async.while(
				function(error, results, callback) {
					i++
					callback(undefined, i <= N)
				} )
			.do(
					{
						'call':function(callback) {
							var t0 = millisec()
							
							var mb = _this.dataSource.getNextBatch()
								.then(function(mb) {
									return (_this.taskmanager.exec(_this.minibatch(mb), _this))
								})
								.then(
									function(rst) {
										var t1 = millisec()
										logger.info("Job, run, this: " + _this + ", rst: " + JSON.stringify(rst) + ", duration: " + (t1 - t0))
										callback(undefined, i)
									}).catch(
											function(error) {
												logger.error("Job, run, this: " + _this + ", error: " + JSON.stringify(error))
												callback(undefined, i)
											}
									)
						}
					})
					.exec( function(error, results) {
						logger.info("JobDL, run, this: " + _this + ", N: " + N)
					});
	}
}

module.exports = JobDL;
