/**
 * http://usejsdoc.org/
 */

var express = require('express');
var router = express.Router();
var path = require("path");
var fs = require('fs');
var logger = require('log4js').getLogger();

/**
 * 
 * @param req
 * @param res
 * @param next
 * @returns
 */
router.get("/", function(req, res, next) {
	var jobManager = global.JobManager
	var taskManager = jobManager.getTaskManager()

	var rst = "\n"
	rst += "getSubmittedTask:\n"
	for ( var key in taskManager.getSubmittedTask()) {
		rst += taskManager.getSubmittedTask()[key].info()
		rst += "\n"
	}
	rst += "\n"
	rst += "getCompletedTask:\n"
	taskManager.getCompletedTask().forEach(function(element) {
		rst += element.info()
		rst += "\n"
	});
	rst += "\n"
		rst += "getInErrorTask:\n"
		taskManager.getInErrorTask().forEach(function(element) {
			rst += element.info()
			rst += "\n"
		});
	rst += "\n"

	res.end(rst);
});

module.exports = router;
