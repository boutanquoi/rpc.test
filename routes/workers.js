/**
 * http://usejsdoc.org/
 */

var express = require('express');
var router = express.Router();
var path = require("path");
var fs = require('fs');
var logger = require('log4js').getLogger();

/**
 * 
 * @param req
 * @param res
 * @param next
 * @returns
 */
router.get("/", function(req, res, next) {
	var jobManager = global.JobManager
	var resourceManager = jobManager.getTaskManager().getResourceManager()

	var rst = "\n"
	rst += "getWorkers:\n"
	for ( var key in resourceManager.getWorkers()) {
		rst += resourceManager.getWorkers()[key].info()
		rst += "\n"
	}
	rst += "\n"
	rst += "getAvailable:\n"
	resourceManager.getAvailable().forEach(function(element) {
		rst += element.toString()
		rst += "\n"
	});
	rst += "\n"

	res.end(rst);
});

module.exports = router;
