/**
 * http://usejsdoc.org/
 */

var express = require('express');
var router = express.Router();
var path = require("path");
var fs = require('fs');
var logger = require('log4js').getLogger();

/**
 * 
 * @param req
 * @param res
 * @param next
 * @returns
 */
router.get('/', function(req, res, next) {
	var filename = path.resolve('test/index.html');
	res.sendFile(filename);
});

module.exports = router;
