/**
 * http://usejsdoc.org/
 */

var express = require('express');
var router = express.Router();
var path = require("path");
var fs = require('fs');
var logger = require('log4js').getLogger();

var Job1 = require('../test/job1')
var JobDL = require('../test/job_deeplearning.js')

/**
 * 
 * @param req
 * @param res
 * @param next
 * @returns
 */
router.get("/", function(req, res, next) {
	var jobManager = global.JobManager

	var rst = "\n"
	rst += "getJobs:\n"
	for ( var key in jobManager.getJobs()) {
		rst += jobManager.getJobs()[key].toString()
		rst += "\n"
	}

	rst += "\n"

	res.end(rst);
});

/**
 * 
 * @param req
 * @param res
 * @param next
 * @returns
 */
router.get("/job1", function(req, res, next) {
	var job = global.JobManager.newJob(Job1, req)
	global.JobManager.run(job)
	res.end('Job: ' + job);
});

/**
 * 
 * @param req
 * @param res
 * @param next
 * @returns
 */
router.get("/jobdl", function(req, res, next) {
	var job = global.JobManager.newJob(JobDL, req)
	global.JobManager.run(job)
	res.end('Job: ' + job);
});

module.exports = router;
